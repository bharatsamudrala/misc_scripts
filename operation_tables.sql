CREATE TYPE url_crawl_status AS ENUM ('R','NR', 'WIP', 'C', 'F');

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

SELECT uuid_generate_v4();

create table seed_urls(seedurl_id uuid primary key default uuid_generate_v4(),
	domain_name varchar(200),
	url varchar(500) not null,
	active boolean not null,
	status url_crawl_status default 'NR' not null,
	dynamic boolean not null,
	company_name varchar(500),
	robots_code integer,
	filter varchar(100),
	cretedby varchar(100),
	created timestamptz,
	updatedby varchar(100),
	updated timestamptz),
	manual boolean;

create table url_attributes(seedurl_id uuid references seed_urls(seedurl_id),
	download_location varchar(500),
	processmeta varchar(1000),
	url varchar(500),
	http_status varchar(10),
	filename varchar(100),
	rundate timestamptz DEFAULT now(),
	content_length integer,
	last_modified varchar(100),
	cretedby varchar(100),
	created timestamptz,
	updatedby varchar(100),
	updated timestamptz);

create table spider_trace(spider_opened varchar(50),
	spider_closed varchar(50),
	spider_name varchar(100),
	server_name varchar (100),
	total_runtime varchar(50));